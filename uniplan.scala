import net.fortuna.{ ical4j => ical }
import net.fortuna.ical4j._
import net.fortuna.ical4j.model._
import net.fortuna.ical4j.model.component._
import net.fortuna.ical4j.model.parameter._
import net.fortuna.ical4j.model.Dur._
import net.fortuna.ical4j.model.property._
import net.fortuna.ical4j.util._

// don't assume gmt, use local timezone
System.setProperty("net.fortuna.ical4j.timezone.date.floating", "true")
val Beschreibung = "Quelle: http://www.uni-muenster.de/studium/orga/termine.html"

case class Semestertermin(Name: String, Semesterbeginn: String, Vorlesungsbeginn: String, ErsterFerientag: String, LetzterFerientag: String, Vorlesungsende: String, Semesterende: String)

def constructEvent(name: String, Semester: String, Date: String) = {
  val event = new VEvent(new Date(sdf.parse(Date)), name + " " + Semester)
  // Generate a UID for the event..
  event.getProperties().add(new Uid((name + Semester).replaceAll(" ", "").replaceAll("/", "") + "@terminefristen.uni-muenster.de"));
  //set transparency to transparent, so events don't become "blockers", aka. "invisible to free-busy searches"
  event.getProperties().add(net.fortuna.ical4j.model.property.Transp.TRANSPARENT)
  // set description
  event.getProperties().add(new Description(Beschreibung));
  event
}

val liste: List[Semestertermin] = List(
  Semestertermin("WiSe 2015/16", "01.10.2015", "19.10.2015", "23.12.2015", "06.01.2016", "12.02.2016", "31.03.2016"),
  Semestertermin("SoSe 2016", "01.04.2016", "11.04.2016", "17.05.2016", "20.05.2016", "22.07.2016", "30.09.2016"),
  Semestertermin("WiSe 2016/17", "01.10.2016", "17.10.2016", "23.12.2016", "06.01.2017", "10.02.2017", "31.03.2017"),
  Semestertermin("SoSe 2017", "01.04.2017", "18.04.2017", "06.06.2017", "09.06.2017", "28.07.2017", "30.09.2017"),
  Semestertermin("WiSe 2017/18", "01.10.2017", "02.10.2017", "27.12.2017", "05.01.2018", "26.01.2018", "31.03.2018"),
  Semestertermin("SoSe 2018", "01.04.2018", "09.04.2018", "22.05.2018", "25.05.2018", "20.07.2018", "30.09.2018"),
  Semestertermin("WiSe 2018/19", "01.10.2018", "01.10.2018", "21.12.2018", "04.01.2019", "25.01.2019", "31.03.2019"),
  Semestertermin("SoSe 2019", "01.04.2019", "01.04.2019", "11.06.2019", "14.06.2019", "12.07.2019", "30.09.2019"),
  Semestertermin("WiSe 2019/20", "01.10.2019", "07.10.2019", "23.12.2019", "06.01.2020", "31.01.2020", "31.03.2020"),
  Semestertermin("SoSe 2020", "01.04.2020", "06.04.2020", "02.06.2020", "05.06.2020", "17.07.2020", "30.09.2020"),
  Semestertermin("WiSe 2020/21", "01.10.2020", "05.10.2020", "23.12.2020", "06.01.2021", "29.01.2021", "31.03.2021"),
  Semestertermin("SoSe 2021", "01.04.2021", "12.04.2021", "25.05.2021", "28.05.2021", "23.07.2021", "30.09.2021"),
  Semestertermin("WiSe 2021/22", "01.10.2021", "04.10.2021", "24.12.2021", "07.01.2022", "28.01.2022", "31.03.2022"),
  Semestertermin("SoSe 2022", "01.04.2022", "04.04.2022", "07.06.2022", "10.06.2022", "15.07.2022", "30.09.2022"),
  Semestertermin("WiSe 2022/23", "01.10.2022", "04.10.2022", "23.12.2022", "06.01.2023", "27.01.2023", "31.03.2023"),
  Semestertermin("SoSe 2023", "01.04.2023", "03.04.2023", "30.05.2023", "02.06.2023", "14.07.2023", "30.09.2023"),
  Semestertermin("WiSe 2023/24", "01.10.2023", "09.10.2023", "21.12.2023", "05.01.2024", "02.02.2024", "31.03.2024"));
val sdf = new java.text.SimpleDateFormat("dd.MM.yyyy");

val calendar = new Calendar();
val registry = TimeZoneRegistryFactory.getInstance().createRegistry();
// To add a VTimeZone definition to your calendar you would do something like this:
val tz = registry.getTimeZone("Europe/Berlin").getVTimeZone();
val timeZone = new net.fortuna.ical4j.model.TimeZone(tz);

//calendar.getComponents().add(tz);

calendar.getProperties().add(ical.model.property.Version.VERSION_2_0);
calendar.getProperties().add(new ical.model.property.ProdId("-//uni-muenster.de-fristen-termine-2015//EN"));
calendar.getProperties().add(ical.model.property.CalScale.GREGORIAN);
calendar.getProperties().add(ical.model.property.Method.PUBLISH)
calendar.getProperties().add(new XProperty ("X-WR-CALNAME", "Uni-MS Fristen und Termine 2015-2023"));

for (eintrag <- liste) {
  calendar.getComponents().add(constructEvent("Semesterbeginn", eintrag.Name, eintrag.Semesterbeginn));
  calendar.getComponents().add(constructEvent("Vorlesungsbeginn", eintrag.Name, eintrag.Vorlesungsbeginn));
  calendar.getComponents().add(constructEvent("Vorlesungsende", eintrag.Name, eintrag.Vorlesungsende));
  calendar.getComponents().add(constructEvent("Semesterende", eintrag.Name, eintrag.Semesterende));

  val ferientyp = if (eintrag.Name contains "SoSe") { "Pfingstferien" } else { "Winterferien" }
  // moodle apparently needs a timezone for multi-day events, never wrong to specify one
  val ferien = new VEvent();
  //set the start date.
  val dtStart = new DtStart(new DateTime(sdf.parse(eintrag.ErsterFerientag)));
  dtStart.setTimeZone(timeZone);
  ferien.getProperties().add(dtStart);

  val dtEnd = new DtEnd(new DateTime((new Date(sdf.parse(eintrag.LetzterFerientag))).getTime() + (1000 * 60 * 60 * 24))); //letzter Ferientag muss eins höher gezählt werden
  dtEnd.setTimeZone(timeZone);
  ferien.getProperties().add(dtEnd);
  ferien.getProperties().add(new Summary(ferientyp + " " + eintrag.Name));

  // Generate a UID for the event..
  ferien.getProperties().add(new Uid((ferientyp + eintrag.Name).replaceAll(" ", "").replaceAll("/", "") + "@terminefristen.uni-muenster.de"));
  ferien.getProperties().add(new Description(Beschreibung));
  //set transparency to transparent, so events don't become "blockers", aka. "invisible to free-busy searches"
  ferien.getProperties().add(net.fortuna.ical4j.model.property.Transp.TRANSPARENT)
  calendar.getComponents().add(ferien);
}

import java.io.FileOutputStream
val fout = new FileOutputStream("uni-ms-termine-2015-2023.ics");

//to allow publish without organizer
net.fortuna.ical4j.util.CompatibilityHints.setHintEnabled(net.fortuna.ical4j.util.CompatibilityHints.KEY_RELAXED_VALIDATION, true)

import net.fortuna.ical4j.data.CalendarOutputter
val outputter = new CalendarOutputter();
outputter.output(calendar, fout);

println("fertig")
